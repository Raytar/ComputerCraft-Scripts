-- listen to messages from the dig program
print("Listening for messages. Press CTRL+T to exit.")

for n,side in ipairs(rs.getSides()) do
    if peripheral.isPresent(side) and peripheral.getType(side) == "modem"
        and not rednet.isOpen(side) then
        rednet.open(side)
    end
end

while true do
    local id, message = rednet.receive("DIG")
    print(message)
end