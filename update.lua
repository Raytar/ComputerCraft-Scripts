-- software updater!
arg = {...}

function downloadFile(path)
    fs.delete(path)
    shell.run("wget", baseurl .. filesurl .. "/" .. textutils.urlEncode(path) .. raw .. ref, path)
end

baseurl = "https://gitlab.com/api/v4/projects/7535547/repository"
filesurl = "/files"
treeurl = "/tree"
raw = "/raw"
ref = "?ref=master"

if not fs.exists("lib") then
    fs.makeDir("lib")
    if not fs.exists("lib/json.lua") then
        downloadFile("lib/json.lua")
    end
end

os.loadAPI("lib/json.lua")

if #arg > 0 then
    for i = 1, #arg do
        downloadFile(arg[i])
    end
else
    tree = json.decode(http.get(baseurl .. treeurl).readAll())

    for k, item in pairs(tree) do
        if item.type == "blob" then
            downloadFile(item.path)
        end
    end
end