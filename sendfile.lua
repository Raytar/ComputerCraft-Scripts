function sendMessage(message)
    if global then
        rednet.broadcast(message, protocol)
    else
        rednet.send(id, message, protocol)
    end
end

arg = {...}

for n,side in ipairs(rs.getSides()) do
    if peripheral.isPresent(side) and peripheral.getType(side) == "modem"
        and not rednet.isOpen(side) then
        rednet.open(side)
    end
end

if #arg < 1 then
    print("Usage: " .. shell.getRunningProgram()
        .. " [Computer id OR '-g' for global] [List of files]")
    return
end

id = nil
global = false
protocol = "FTP"

if arg[1] == "-g" then
    global = true
else
    id = tonumber(arg[1])
end

-- send number of files
sendMessage(tostring(#arg - 1))

for i = 2, #arg do
    if fs.exists(arg[i]) then
        local file = fs.open(arg[i], "r")
        sendMessage(arg[i] .. "\n\n" .. file.readAll())
        print("File '" .. arg[i] .. "' was sent.")
        file.close()
    else
        print("File '" .. arg[i] .. "' does not exist!")
    end
end