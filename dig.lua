-- #region functions
-- Function Definitions
function sendMessage(message)
    local name = os.getComputerLabel()
    local text = name .. ": " .. message
    rednet.broadcast(text, protocol)
    if URL then
        http.request(URL, "message=" .. textutils.urlEncode(text))
    end
    -- also print message to screen
    print(message)
end

function safeError()
    sendMessage("Script is paused. Actions required.")
    while true do
        print("Press [c] to continue, [d] to deposit items, [r] to return to start, or [e] to exit")
        local event, key = os.pullEvent("key")
        if key == keys.c then
            sendMessage("Continuing")
            return
        elseif key == keys.d then
            sendMessage("Depositing items")
            deposit(true)
            return
        elseif key == keys.r then
            sendMessage("Returning to start")
            deposit(false)
            error()
        elseif key == keys.e then
            sendMessage("Exiting")
            error()
        else
            print("Invalid key.")
        end
    end
end

function checkInventory()
    local free = 0
    for i = 1, 16 do
        if turtle.getItemCount(i) == 0 then
            free = free + 1
        end
    end
    return free
end

function turnLeft()
    turtle.turnLeft()
    if rotation <= 0 then
        rotation = 3
    else
        rotation = rotation - 1
    end
end

function turnRight()
    turtle.turnRight()
    if rotation >= 3 then
        rotation = 0
    else
        rotation = rotation + 1
    end
end

function turnTo(direction)
    local clockwise = true
    if direction == rotation then
        return
    elseif direction < rotation then
        clockwise = false
    end

    while rotation ~= direction do
        if clockwise then
            turnRight()
        else
            turnLeft()
        end
    end
end


-- If forward == true, then move forward. If false, move backward
function moveH(forward)
    local inc = 0
    if forward then
        safeMove(turtle.forward, retry_count)
        inc = 1
    else
        safeMove(turtle.backward, retry_count)
        inc = -1
    end
    
    if rotation == 0 then --north
        Y = Y + inc
    elseif rotation == 1 then --east
        X = X + inc
    elseif rotation == 2 then --south
        Y = Y - inc
    elseif rotation == 3 then --west
        X = X - inc
    end
end

function moveV(up)
    if up then
        safeMove(turtle.up, retry_count)
        Z = Z + 1
    else
        safeMove(turtle.down, retry_count)
        Z = Z - 1
    end
end

function zeroXDiff(x, xDiff)
    if xDiff == 0 then
        return 
    end
    local direction = 1
    if X > x then
        direction = 3
    end
    turnTo(direction)
    for i = 1, xDiff do
        moveH(true)
    end
end

function zeroYDiff(y, yDiff)
    if yDiff == 0 then
        return
    end
    local direction = 0
    if Y > y then
        direction = 2
    end
    turnTo(direction)
    for i = 1, yDiff do
        moveH(true)
    end
end

function zeroZDiff(z, zDiff)
    if zDiff == 0 then
        return
    end
    local up = true
    if Z > z then
        up = false
    end
    for i = 1, zDiff do
        moveV(up)
    end
end

function moveTo(x, y, z, reverse)
    print(string.format("Moving to %d, %d, %d", x, y, z))
    -- compute path and fuel requirements
    -- always digging left to right, so to get back we have to step east
    -- and then south
    local xDiff = math.abs(X - x)
    local yDiff = math.abs(Y - y)
    local zDiff = math.abs(Z - z)
    local distance = xDiff + yDiff + zDiff
    refuelFor(distance)

    if reverse then
        zeroYDiff(y, yDiff)
        zeroXDiff(x, xDiff)
        zeroZDiff(z, zDiff)
    else
        zeroZDiff(z, zDiff)
        zeroXDiff(x, xDiff)
        zeroYDiff(y, yDiff)
    end
end

function ret()
    -- return to 0, 0, 0
    print(string.format("Position is %d, %d, %d", X, Y, Z))
    sendMessage("Returning...")
    moveTo(0, 0, 0, false)
end

function safeMove(moveFun, numAttempts)
    local success = moveFun()
    if not success then
        local inspect, metadata = turtle.inspect()
        if inspect and metadata.name ~= "minecraft:water" and metadata.name ~= "minecraft:lava" then
            safeDig(turtle.dig, 1)
            safeMove(moveFun, numAttempts)
            return
        end
        if turtle.getFuelLevel() == 0 then
            safeRefuel(1, 16)
            safeMove(moveFun, numAttempts)
            return
        end
        print("Failed to move!")
        if numAttempts > 0 then
            os.sleep(5)
            print("Retrying...")
            safeMove(moveFun, numAttempts - 1)
        else
            sendMessage("Failed to move, too many attempts!")
            safeError()
            safeMove(moveFun, retry_count)
        end
    end
end

function deposit(_return)
    local oldX = X
    local oldY = Y
    local oldZ = Z
    local oldRotation = rotation
    ret()
    turnTo(2)
    for i = 1, 16 do
        turtle.select(i)
        itemData = turtle.getItemDetail()
        if itemData and itemData.name ~= fuelType then
            turtle.drop()
        end
    end
    turnTo(0)
    if _return then
        moveTo(oldX, oldY, oldZ, true)
        turnTo(oldRotation)
    end
end

function safeDig(digFun, numAttempts)
    if freeSlots == 0 then
        if numAttempts == 0 then
            deposit(true)
        end
        freeSlots = checkInventory()
        safeDig(digFun, numAttempts - 1)
    else
        digFun()
        freeSlots = freeSlots - 1
    end
end

function safeRefuel(amount, numAttempts)
    local function tryAgain(newAmount, remainingAttempts)
        fuelSlot = fuelSlot + 1
        if fuelSlot > 16 then
            fuelSlot = 1
        end
        safeRefuel(newAmount, remainingAttempts - 1)
    end

    if numAttempts == 0 then
        sendMessage("Bingo fuel.")
        safeError()
        safeRefuel(amount, 16)
    end

    -- check for the correct fuel type
    turtle.select(fuelSlot)
    local itemData = turtle.getItemDetail()
    if not itemData or (itemData and itemData.name ~= fuelType) then
        tryAgain(amount, numAttempts)
    else
        -- try to refuel
        local amountAvailable = turtle.getItemCount()
        if not turtle.refuel(amount) then
            tryAgain(amount, numAttempts)
        else
            -- make sure we got the amount we wanted
            if amountAvailable < amount then
                amount = amount - amountAvailable
                tryAgain(amount, numAttempts)
            end
        end
    end
end

function refuelFor(numMoves)
    local fuelLevel = turtle.getFuelLevel()
    if fuelLevel <= numMoves then
        fuelNeeded = math.ceil((numMoves - fuelLevel) / fuelEfficiency)
        safeRefuel(fuelNeeded, 16)
    end
end

function setPosition(x, y, z)
    X = x
    Y = y
    Z = z
end

function dig(Width, Height, Depth)
    sendMessage("Starting")
    print("Hold 'CTRL+T' to stop.")
    os.sleep(3)

    for z = 1, Depth do
        if Width * (Height + 1) > turtle.getFuelLevel() then
            refuelFor(math.ceil(Width * (Height + 1) / fuelEfficiency))
        end
        for x = 1, Width do
            counter = counter + 1
            for y = 2, Height do
                safeDig(turtle.dig, 1)
                moveH(true)
            end
            if x == Width then
                turnRight()
                turnRight()
            elseif (counter + z % 2) % 2 == 0 then
                turnRight()
                safeDig(turtle.dig, 1)
                moveH(true)
                turnRight()
            else
                turnLeft()
                safeDig(turtle.dig, 1)
                moveH(true)
                turnLeft()
            end
        end
        if z < Depth then
            safeDig(turtle.digDown, 1)
            moveV(false)
        end
    end
    deposit(false)
    sendMessage("Done!")
end
-- #endregion

-- get configuration
os.loadAPI("lib/json.lua")
local configFile = "dig_config.json"
URL = nil
if fs.exists(configFile) then
    local config = json.decodeFromFile("dig_config.json")
    if config then
        URL = config.URL
    end
end

-- Init global variables

-- protocol used for rednet communication
protocol = "DIG"

-- enable rednet
for n,side in ipairs(rs.getSides()) do
    if peripheral.isPresent(side) and peripheral.getType(side) == "modem"
        and not rednet.isOpen(side) then
        rednet.open(side)
    end
end

local arg = {...}
-- make sure we don't error if running as API
if #arg ~= 3 and shell ~= nil then
    print(string.format("USAGE: %s [Width] [Height] [Depth]", shell.getRunningProgram()))
    return
end

-- how many times to retry an action if it fails
retry_count = 15

-- position data
X = 0
Y = 0
Z = 0
rotation = 0

-- counts the number of lines mined
counter = 0

-- check the inventory
freeSlots = checkInventory()

-- change to slot 1 for fuel
fuelSlot = 1
turtle.select(fuelSlot)

-- determine the type of fuel used
fuelType = nil
do
    local data = turtle.getItemDetail()
    if data then
        fuelType = data.name
    else
        print("No fuel. Please put fuel in the first inventory slot.")
        error()
    end
end

-- determine the efficiency of the fuel. defaults to that of coal
fuelEfficiency = 80
do
    local oldFuelLevel = turtle.getFuelLevel()
    if not turtle.refuel(1) then
        print("Invalid fuel.")
        error()
    end
    local newFuelLevel = turtle.getFuelLevel()
    fuelEfficiency = newFuelLevel - oldFuelLevel
    print("Fuel efficiency is: " .. tostring(fuelEfficiency))
end

-- exit now if we are running as an API
if shell == nil then
    return
end

-- Main program

-- setting variables
local Width=tonumber(arg[1])
local Height=tonumber(arg[2])
local Depth=tonumber(arg[3])

-- start
dig(Width, Height, Depth)
