for n,side in ipairs(rs.getSides()) do
    if peripheral.isPresent(side) and peripheral.getType(side) == "modem"
        and not rednet.isOpen(side) then
        rednet.open(side)
    end
end

protocol = "FTP"

local id, message = rednet.receive(protocol)
num_files = tonumber(message)

for i = 1, num_files do
    local id, message = rednet.receive(protocol)
    local split = string.find(message, '\n\n')
    local fileName = string.sub(message, 1, split - 1)
    local contents = string.sub(message, split + 2)
    local file = fs.open(fileName, "w")
    file.write(contents)
    file.close()
    print("File '" .. fileName .. "' received.")
end